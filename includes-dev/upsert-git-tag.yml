upsert git tag:
  image: node:17.1-alpine3.14
  stage: tag
  variables:
    GIT_STRATEGY: "fetch"
  script:
    - apk update && apk add jq git curl
    - npm install -g changelog-parser
    - CHANGELOG_VERSION=$(changelog-parser | jq -r '.versions[0].title')
    - CHANGELOG_MESSAGE=$(changelog-parser | jq '.versions[0].body')

    - TAG_EXISTS=$(git tag -n | grep "^$CHANGELOG_VERSION\s\+" || true)
    - |
      RED='\033[0;31m'
      NC='\033[0m' # No Color

      if [ -z "$GITLAB_TOKEN" ]; then
        echo -e "${RED}Error while attempting to tag and release new version: 'GITLAB_TOKEN' environment variable must be configured.${NC}"
        exit 1
      fi

      if [ -n "$TAG_EXISTS" ]; then
        echo "Changelog version '$CHANGELOG_VERSION' found in git tags, skipping new release creation."
        exit
      fi

      echo "Changelog version '$CHANGELOG_VERSION' not found in git tags, tagging and releasing new version '$CHANGELOG_VERSION' with description:"
      echo "  $CHANGELOG_MESSAGE"
      echo ""

      echo "Creating tag..."

      # disable "Exit immediately" behaviour so we can provide a more informative error message
      set +e
      curl --fail-with-body -o response-body.txt --silent --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
           --header "Content-Type: application/json" \
           "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases" \
           --data "
           {
             \"tag_name\": \"${CHANGELOG_VERSION}\",
             \"ref\": \"${CI_COMMIT_BRANCH}\",
             \"name\": \"${CHANGELOG_VERSION}\",
             \"description\": ${CHANGELOG_MESSAGE}
           }"

      curl_exit_code=$?
      set -e

      if [ $curl_exit_code -ne 0 ]; then
        echo -e "${RED}Error while attempting to tag and release new version: curl command failed with exit code ${curl_exit_code}.${NC}"
        echo "Response body: $(cat response-body.txt)"
        exit 1
      fi

      echo ""
      echo "Tag successfully created. Curl response:"
      jq 'del(.commit.author_name, .commit.author_email, .commit.committer_name, .commit.committer_email, .author)' response-body.txt
  rules:
    - if: '($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^v\d$/) && $CREATE_GIT_TAG'
